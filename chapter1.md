# Chapter 1 - The Idea

In the beginning of every Mod stands an idea. And usually it is pretty hard to
find ideas that are not yet implemented by other developers. I will - for the
purposes of showing Forge to you - not use a very original idea.

Maybe you found this tutorial because you had a mod idea and want to make it
reality? If so, then I am really happy. Because this tutorial is for you.
And before I tell you what the mod in this tutorial will be, I want you to give
your own idea some thoughts. And since there is no strict tutorial for such
a creative process, I will just give my own way of thinking about new ideas.

## 1. The Idea
The moment I have a nice idea for a mod or extension or really anything I could
do, I try to write it down. Maybe it's just some scribbles, maybe it's something
more full-fledged, a chart, a text, an image? It doesn't really matter.

I try to make my idea more and more concrete by thinking things and mechanics
through. Make yourself a plan. In the case of Minecraft Mods, maybe think of
things regarding it's properties. What new blocks/items do I want? How are they
crafted/obtained? And often enough I just start programming and trying things
out in game - I program something, look how it looks in-game and then refine.

## 2. This Tutorials Idea
Now want you to put back your own idea and get accustomed to this new one. I
want to warn you, it is not an original one. It is a mod with new materials
that expand the basic materials for toolmaking and armory by sapphire. There
are several reasons for that. Basically, I want to keep my mod as simple and
easy to understand as possible while still covering most of the different
ideas and concepts going into modding. Using these basic stuff, it also keeps
textures and assets pretty close to Minecraft vanilla. This makes it easier to
compare. I still want to cover more complex ideas than just basic stuff but
we'll stick with the new material **Sapphire** for now.

## ~ Now You!
This section will be at the end of every chapter or even at the end of some
sections from now on. I want to keep these chapters as interactive as possible
whilst not discouraging anyone who fails to do these exercises. Therefor there
will be a small hint section for every task there is. There will also be
extended solutions with explanations which can be found on separate pages.
Since this chapter is creative, the task will be too. Use the hints provided to
have some extra input or don't read them at all.

**This chapter's task:**
Think of possible textures for a sapphire (the gem, not the ore or the block).
You can just scribble them down on a small paper, digitally, or even create a
final 16x16 texture for them.

**Hints - Next chapter's hints will be much more secluded. Read only if you
want to read them!**
 - Sapphires are deep blue. (Yes, these hints will be very creative)
 - Maybe you want sapphires looking like diamonds. Or more like an ingot. Or
   maybe create a completely new look to them.
 - If you are looking for inspiration you can look at the Google Images results
   for [sapphire](https://www.google.com/search?q=sapphire&tbm=isch) and/or
   [sapphire minecraft](https://www.google.com/search?q=sapphire+minecraft&tbm=isch)

### Solution
This is a creative task. There is no real solution. Here only short explanations
will be shared in other chapters. For this chapter you should just have any
graphic representing a sapphire in your mod.
