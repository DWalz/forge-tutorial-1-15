# Chapter 5 - Item Groups and `static Item`

There is much more about item creation than first meets the eye. Usually, if we
want to give an item a functionality that isn't normal and therefor it's
behavior is not defined by vanilla Minecraft, we would create a new class for
exactly that item that would define it's functionality. Since our sapphire is
no such item, we will - for now - create no such class. This will be reserved
for sapphire tools that will come with some special abilities.

In this chapter we'll look into how we can create items in other ways - this will
mainly make things easier for registering all the future items we want to create
and how to create an item group, so our sapphire is visible in the creative
inventory under it's specialized tab just for our mod.



## Forge predefined items

If we actually take a look into the Forge code of the `Item` class
(in IntelliJ you can do this by `Ctrl` + `Click` on the class anywhere in your code)
we'll find that almost the full class is just `public static final Item ...`
declarations. These declarations register the standard Minecraft items to Forge.

This is the code for the diamond registration
(since the diamond is similar to what we want a sapphire to be):

```java
public static final Item DIAMOND = register("diamond", new Item((new Item.Properties()).group(ItemGroup.MATERIALS)));
```

What this does is essentially registering as standard item with standard
properties to the materials tab in the creative menu. We'll adapt this
registration for our own items. Since the `regsiter()` function is `private`,
we can't use it - we still have to go the way through the `Register<Item>` event.

Luckily, the `Register<Item>` event comes with a big timesaver: `registerAll()`.
We can just use this function to register a whole bunch of items we previously
created.

So what are we actually going to do now? We could create a list of all the items
we have in our mod and then just throw the whole list into the `registerAll()`
function. This way will make it very easy to create new items: We just have to
add them to the list. But there is one disadvantage to that. Sometimes, you need
to actually access some items by name. We want to be able to still get that item
and maybe test if an item the player is crafting with it that exact item. To
still be able to do that, we'll rather save the items just the way they are
saved in the Forge `Item` class: by name.


### ~ Now You!

 - [ ] Create a `public static final Item SAPPHIRE` and instantiate with a standard
 item and the proper resource location.
 - [ ] Replace the `register()` function in our code with the `registerAll()`
 function and pass it the item.
 - [ ] Since we only call to the registry once now, delete the old `itemRegistry`
 variable

#### Hints
 - The item should be the same as before. We just save it at another point.
 - You can just straight up stick the `SAPPHIRE` item into the `registerAll()`
 function. We may want to add more later but we can just separate them by comma `,`.

#### Solution
The `ItemInit` class should look something like this:

```java
@Mod.EventBusSubscriber(modid = TutorialMod.MOD_ID, bus = Mod.EventBusSubscriber.Bus.MOD)
public class ItemInit {

    public static final Item SAPPHIRE = new Item(new Item.Properties()).setRegistryName("sapphire");

    @SubscribeEvent
    public static void registerItems(final RegistryEvent.Register<Item> event) {
        event.getRegistry().registerAll(
                SAPPHIRE
        );
    }

}
```



## Item Groups

Up until now we could only get our item in game via command. We typed the
*resource location* of our item. The resource location depends on how we
register items. When we call `setRegistryName()` we specify which resource
location we want the item to have - its get built by being attached to the
`modid:`, so in our case usually `tutorialmod:item_name`.

The idea of item groups in the creative menu is very separate from this. The
resource location of our item is like the identifier that tells Minecraft
where our item is specified while item groups are purely visual and made
for us humans to categorize our items. Forge's class `ItemGroup` comes -
just like the `Block` class with several pre-built item groups: the Minecraft
standard item groups.

We could just use these pre-build item groups to sort our items in there but
often we'll want to have our separate item groups for our mods. Some big mods
even go all the way and categorize their items in several item groups. We can
create new item groups by just extending the class.

**Quick reminder:** The class `ItemGroup` is an `abstract class`. In Java this
means we have to override all methods that are declared in that class and marked
with `abstract` but not defined (they only have their header (`abstract`), not
the implementation). We mark them in *our* class with the `@Override` annotation.

Since `ItemGroup` has only one method we have to override - `createIcon()`,
we can do this on the fly. Since we do all item registration stuff in our
`ItemInit` class we can just create the `ItemGroup` there as `public static final`
variable and re-use it whenever we need to.


### The `createIcon()` method - `Item`s and `ItemStack`s

The `createIcon()` method is used to give our `ItemGroup` an icon - just like
the name says. It uses an `ItemStack` to render the icon. Since an `Item` and
an `ItemStack` may be considered the same, I'll go over the differences here
quickly:

If I try to say it as simplistic as I can: An `Item` is the concept of an item
while an `ItemStack` is one or more objects of this item type. If we try to
explain this in terms of our sapphire: The `Item` is what we defined in our
code: It is the pointers to the resource location, what texture this item should
have, which name and which functionality. As soon as we gave us a sapphire
in game via command, we got an `ItemStack` of that `Item` sapphire.

*Example:* `ItemStack`s can contain multiple items - the standard Minecraft
stack can stack up to 64 items. But the information about how big we can stack
is defined in the item. We could - and will - set it to other values and then
this `Item`'s `ItemStack`s would behave differently.

So `Item` and `ItemStack` are corelated but no the same.

To now create our group logo the `createLogo()` function has to return an
`ItemStack`. Luckily, since `Item` and `ItemStack` are so similar, we can just
create an `ItemStack` by using a constructor and our `Item SAPPHIRE`.

If we take a look at the three `ItemStack` contructors we can see that we could
put multiple items in this stack ore even change the *NBT data* (later!) of
the stack. Since we don't want an index number to be in the logo and it'd be
unnecessary to add some extra data (we can never get the item stack out in game)
we'll just use the easiest one: `ItemStack(SAPPHIRE)`

```java
public ItemStack(IItemProvider itemIn)

public ItemStack(IItemProvider itemIn, int count)

public ItemStack(IItemProvider itemIn, int count, @Nullable CompoundNBT capNBT)
```


### Settiong the `ItemGroup` of an `Item`

If we have created our `ItemGroup` we can place our `Item` in it. This is done
in the items `properties`. We can just set the group of our `Item` by calling
the `group()` function on the `Item`'s `Properties`. Pretty straightforward.


### ~ Now You!
 - [ ] Create a new `public static final ItemGroup MOD_GROUP` variable and
 create our mod's `ItemGroup` there. It should have the label `tutorialmod`.

 - [ ] Implement the `createIcon()` method by returning an `ItemStack` of the
 sapphire `Item`.

 - [ ] Change the saapphire `Item`'s `ItemGroup` by calling the `group()`
 function on it's `Properties`.

 - [ ] Add a fitting translation string in your datapack's `lang` file.

#### Hints
Yes, these tasks are pretty hard. Therefor there will be more hints and
explaining here. Don't worry if you don't get the solution. You can look it up
and these tasks are made to challenge you.

 - The `ItemGroup`'s constructor takes a `String label` as argument. This is
 similar to the registry names of the items.

 - If we didn't have to implement `createIcon()` our contructor would look like
 this:

 ```java
 public static final ItemGroup MOD_GROUP = new ItemGroup("tutorialmod");
 ```

 - We can override the method `createIcon()` without creating a completely
 new class and name it something. This is done by the creation of an *anonymous
 class*. Just after the call of the constructor we add the class' body:

 ```java
 public static final ItemGroup MOD_GROUP = new ItemGroup("tutorialmod") {
        @Override
        public ItemStack createIcon() {
        }
    };
```

 mostly, your IDE will auto-generate that code if you ask it to (hover over
 the errors and select `Implement Methods`).

 - You have to modify the constructor of our `SAPPHIRE` `Item` to change the
 group to `MOD_GROUP`

 - You can find out how the translation string has to be named by starting the
 game and looking at your item group. Untranslated strings are just written out
 by their names.

#### Solution

```java
@Mod.EventBusSubscriber(modid = TutorialMod.MOD_ID, bus = Mod.EventBusSubscriber.Bus.MOD)
public class ItemInit {

    public static final ItemGroup MOD_GROUP = new ItemGroup("tutorialmod") {
        @Override
        public ItemStack createIcon() {
            return new ItemStack(SAPPHIRE);
        }
    };

    public static final Item SAPPHIRE = new Item(new Item.Properties().group(MOD_GROUP)).setRegistryName("sapphire");

    @SubscribeEvent
    public static void registerItems(final RegistryEvent.Register<Item> event) { ... }

}
```

And the key-value pair

```
  "itemGroup.tutorialmod": "Tutorial Mod"
```

in the `en_us.json` file in the mod's datapack.



## End words

Congratulations on making it this far. You now know anything there is to create
basic items and make them fit into the game. If you take a look at the
`Item.Properties` class you will even pretty easily be able to modify some
properties of your items and item stacks.

We will now move away from pure items and create a sapphire block next.
Blocks have a slightly different workflow and there are some concepts that are
important to understand to create blocks. It won't be much harder than creating
items - I promise.
