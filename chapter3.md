# Chapter 3 - Building an empty Mod
In this chapter we're going to accustom ourselves to how Forge works and which
basic files are needed to build a mod. We'll delete most of the things that the
example mod created and rewrite them the moment Forge needs them.

## 1. The file structure of Forge
In your IDE there will be many files and folders in the current project. Most
of (in fact almost all of them) are completely irrelevant for the development
of our mod. We only need one folder: The `src` folder. This is where all of
the files for our mod are stored. The other files are just project management
and build tools we really don't need to worry about. Inside of the `src` folder
there is a `main` folder and inside of that folder are the two folders that make
our mod: `java` and `resources`.

The `java` folder contains all the java code of our mod. Here we'll specify new
blocks, define how they work and so on. It is the game-logical part of our mod.

The `resource` folder contains the files like texture and model files. It is
the *visible* part of our mod.

If you open all folders and packages, you will find that the hierarchy of the
files looks like this:

```
src/
  |- main/
    |- java/
    | |- com/example/examplemod/
    |   |- ExampleMod.java
    |- resources/
      |- META-INF/
      | |- mods.toml
      |- pack.mcmeta
```
We'll go over all the files and packages in the next parts.

**Note:** In your IDE (Eclipse or IntelliJ or any other) the folder structure
may look not really like what you see
above. This is because your IDE presents your project another way. This is
because Java files are organized in so-called *packages*. These packages work
the same way folders do but are separated by a `.`. They are identifiers to
reference to a certain software - if someone would want to include your mod
classes in another mod, they could reference to them via the package.
So `com.example` is the
same as `com/example`. This is simplifying a few things but since we won't
really need to know what packages do, we'll just jump over that for now.

## 2. The `resources` folder
Huh? Why are we talking about the boring artsy stuff first?

Simple. The `resource` folder is much more than just textures and models. In the
resource folder lies the main entry point for the Forge mod loader to start a
mod: The `mods.toml` file.

### `META-INF/mods.toml`
The Forge example mod provides us with a pretty well documented `mods.toml`
file. This file also is the point where many errors happen so it is nice to keep
all the stuff in there and just modify what we need. I'll go over all the
important fields in the file and we'll learn how Forge initially loads mods.

What you can see in the file is the information you could see when you started
the example mod in the last chapter. All our mod information are stored here.

When Forge starts and sees a `.jar` file in it's `mods` folder, it will first
load this file out of the jar and use it to register the mod. It will grab the
`modId` and register it in the internal mod loader. This id now sits there and
waits to be connected to the functional (java) part of the mod. How that works
we'll see later.

I will change my `modId`. The mod id is a unique name. Two mods with the same
id in the same mods folder would crash each other.

The `version` is exactly what it seems to be - the version of our mod. Since
we don't know what `${file.jarVersion}` does, we will hardcode it for now.

The `displayName` is a name string for our mod. This is how players see this
mod.

The fields `credits`, `authors` and `description` are pretty self explanatory.

Other fields in this file are not to be touched. They are used by Forge to build
some stuff and the standard settings are the best for not well-experienced
programmers. We'll leave them untouched.

### `pack.mcmeta`
Every texture or other resource in Minecraft - like item names, and item models
are defined in a resource pack in. This is no exception
for the standard textures or new textures of our Mod. Every resource pack has
a `pack.mcmeta` file describing the pack. We don't need to edit this file for
now since we haven't actually created any items that need textures or so, it
is fine as-is.

### ~ Now You!

#### Task
 - [ ] Change the `mods.toml` file so the mod has an ID of `tutorialmod`, a version
   of `0.1.0` and a name of `Tutorial Mod`. You can fill out the fields
   `credits`, `authors` and `description` to your liking.
 - [ ] Try to start the mod. What happens?
 - [ ] Download the [Default3D 1.15.2 texture pack](https://know2good.com/20w09.php),
   extract and open it in your file explorer. Make yourself accustomed to the
   structure of the pack. We'll use this exact structure for our own blocks
   (if simpler).
   TODO: Insert better resource pack here

#### Hints
 - Look up the fields you need to edit in order to change the requested options
 - Try only to write inside of a defined string `""`
 - When you start Minecraft you'll get an error message. Can you think, why?
 If not: It doesn't matter. This question is pretty hard and the section you'd
 have to look at would be: #
 > When Forge starts and sees a `.jar` file in it's `mods` folder, it will first
 load this file out of the jar and use it to register the mod. It will grab the
 `modId` and register it in the internal mod loader. This id now sits there and
 waits to be connected to the functional (java) part of the mod. How that works
 we'll see later.

#### Solution
Since this is pretty important, here is the finished `[[mods]]` section of the
`mods.toml` file:

```toml
[[mods]] #mandatory
# The modid of the mod
modId="tutorialmod" #mandatory
# The version number of the mod - there's a few well known ${} variables useable here or just hardcode it
version="0.1.0" #mandatory
# A display name for the mod
displayName="Tutorial Mod" #mandatory
# A URL to query for updates for this mod. See the JSON update specification <here>
updateJSONURL="http://myurl.me/" #optional
# A URL for the "homepage" for this mod, displayed in the mod UI
displayURL="http://example.com/" #optional
# A file name (in the root of the mod JAR) containing a logo for display
logoFile="examplemod.png" #optional
# A text field displayed in the mod UI
credits="Your own credits" #optional
# A text field displayed in the mod UI
authors="Your own name" #optional
# The description text for the mod (multi line!) (#mandatory)
description='''
Your own description

I like wolves :)
'''
```

As for the error message: As stated earlier, Forge will register a `modId`
internally and then wants to connect this to the functional part. This is not
possible because we just changed the ID from `examplemod` into `tutorialmod`.
Therefor the previous working functional part that got connected to this ID
now can't connect anymore since we basically renamed the mod.


## 3. The `java` folder
In the `java` folder our mod is defined. Theoretically all we need to have a
working mod is the main mod class that connects the mod to the `modId`
registration done in the `mods.toml` file. So this is what we're going to do.

We'll delete the old package `com.examplemod` and everything in it.

### Our own package
Then we'll create our own package. Packages have specific naming guidelines so
that every package could be relatively easily identified by others. Therefor
we name ours after the virtual website we could be having. This is
best-practice, because if someone sees our package with code and wants to know
who did that, he could just type the package name as URL into the browser.

My website is accessible under www.lonewolf.se so I name the package for all
Java applications I write `se.lonewolf`. Then I create a sub-package of that
with the name of my application, in my case `tutorialmod`.

So in total I have the package `se.lonewolf.tutorialmod`

### The Mod's Main Class
Inside the package `tutorialmod` we want to create the main class of my mod.
It will be named `TutorialMod`.

Our IDE will auto-generate the class for us:
```java
public class TutorialMod {
}
```

**Note:** I won't include my packages and maybe my imports in the future
anymore. This is, because you can easily auto-generate these lines for yourself
and they will take up enormous amounts of space.

This class is for now just a basic java class. We need to tell Forge that this
is a mod class and indeed the mod class with the ID `tutorialmod`. That is done
via Annotations.

#### Annotations
Annotations are the Java way to add metadata to a program or a part of a program
(in our case a class). The annotation will not influence the execution of the
code, it is purely to give information to the compiler or Java at runtime. In our
case we just want to tell Forge that this is the main class of the mod with the
ID `tutorialmod`.

Doing so is quite simple. We just add an Annotation before the class
declaration. Annotations begin with an `@`, followed by the annotation name and
eventual parameters in brackets `()`. In our case we want Forge to know that
this is a `Mod` with the id `tutorialmod` so we'll just annotate
`@Mod("tutorialmod")`:

```java
import net.minecraftforge.fml.common.Mod;

@Mod("tutorialmod")
public class TutorialMod {
}
```

**Note:** We need to import the `Mod` annotation. This is automatically done by
IntelliJ if we press `ENTER` once we see `@Mod` selected in the popup menu while
typing. If there are any errors and the IDE tells you *Cannot resolve symbol* or
*cannot resolve reference*, just hover over the annotation and select
`Import class`.

### Test our Mod
If we now start the client via our IDE again, we should be able to see the mod
running just normally without any errors. We have successfully connected the ID
registration of our mod to a functional counter-part that will now contain our
future mod.

![Our empty mod](chapter3/loadedmod.png)

### ~ Now You!

#### Task
Follow the steps I presented to create your empty mod:
 - [ ] delete all `java` contents
 - [ ] create your own named package
 - [ ] create a new main class for your mod
 - [ ] annotate it so that Forge identifies it as a mod
 - [ ] start Minecraft and see if the error from last chapter is resolved

#### Hints
This should be pretty straightforward, so I won't provide any hints here. If
you run into any errors that you can't resolve by yourself, please feel free
to contact me via e-mail: pythonit01@gmail.com

#### Solution
You should have Minecraft running with your mod and without any errors now.

Also for reference, your base class should now look like this (replace the
package name by your package name):

```java
import net.minecraftforge.fml.common.Mod;

@Mod("tutorialmod")
public class TutorialMod {
}
```


## 4. Disclaimer
I just personally wanted to mention a few things after the first three (or four)
chapters: I am really, really grateful, you still stayed with me. I know, the
beginning is hard. It always is very theoretical and complicated or - for those
of you, who know the stuff I told you - really boring and repetitive.

The problem is: I try to merge multiple levels of knowledge to learn from one
source. And for those who know stuff it is really important to filter the
information that is new and recognize, where I made simplifications and what
is behind them.

**But:** I want to make an announcement for the following chapters: We will now
concentrate on Minecraft and Forge. Yes, I will throw in a few knowledgeable
things about Java and how certain things work, but the main focus will be the
Mod. So heads up and get ready to build your own Minecraft Experience!
