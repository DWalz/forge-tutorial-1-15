
# Chapter 0 - Preface

This chapter won't be unnecessarily long. I will try to keep myself short,
but there are several things I want to give you on the way of this primarily
**written** tutorial of *Minecraft Mod Development with Forge*. There will
be much more reading to do, at the moment of creating this tutorial I really
don't know if I want to create a video tutorial based on this but I still
want this tutorial to be the main tutorial.

## 1. What do I need for this tutorial
I am sorry to say this but I won't go over the basics of programming here.
The purpose of this tutorial is to bring beginners that know how to do some
coding an understanding of more advanced programming concepts and of course
show you, how to work with an API such as Forge to create and modify existing
programs - in other words: Building a mod for a Forge environment.

You will require knowledge of basic programming and object-oriented concepts in
Java. These concepts contain:

 - Variables
 - Functions & Methods
 - Classes
 - Class hierarchy

If you don't recognize any of these items, I'd hardly recommend you look into
the basics of programming. There are excellent resources out there bringing you
closer to programming. I refer to one I like here:
 - [Java Tutorial (YouTube)](https://www.youtube.com/watch?v=eIrMbAQSU34)

 Any more advanced topics will be covered in the tutorial.


## 2. How does a chapter look like?
Every chapter will more or less be based on the same layout. It will feature a
certain topic. We'll start with some introduction, get to the coding and the
realization of things we want to do and then there will be a small challenge
with a solution in every chapter. This is to refactor the things we talked
about and try them on your own. Feel free to E-Mail me with code solutions
or any questions you have. I'll try to respond as quickly as possible.

## 3. What's coming up?
The next few chapters will be a little bit theoretical. I am sorry for that.
But there will be no way around it if you want to *understand* how to create
Forge mods. You could go anywhere and learn a copy-paste like way to create
mods. But that is boring and doesn't help anyone in the long run. We want to
understand the concepts the Forge Mod Loader is bringing with it and how to
find out on your own how some things are meant to be used by the Forge
developers.

I will reference the official Forge docs **a lot**. And there is a reason for
that: You need to see how I refactored the text written by the Forge Development
Team and learn how to use their API to figure out things on your own that are
not covered by this tutorial.
