# Chapter 2 - Installing Forge and Workspace Setup

In this chapter we will set up our programming environment. We will install
Java, an IDE (both Eclipse or IntelliJ, I will be working with IntelliJ), Forge
for Minecraft and create an empty Forge mod. This will be our setup for the
rest of the series. These chapters will include more and more images and
screenshots to make following along easier.

## 1. Installing Java
The Forge Mod Loader is built for Minecraft Java Edition so there really is no
way around Java if you want to do anything with Minecraft. I will firstly
give a short introduction to Java and how it is operating to make you understand
what you are installing and why.

### Java Introduction
Java is a platform-independent programming language. That means you can send
Java programs to every computer and they could run them - theoretically.
A Java program is binary code - binary code that is understood by the Java
Runtime Environment - short JRE. The JRE runs our Java Applications such as
Minecraft. The binary code that defines executes the *same stuff* on every
machine on platform as long as it is run by the *platform-specific JRE*

To create Java Applications we have to use the Java Development Kit - short JDK.
It allows us to create the binary code that can be understood by the JRE. So in
order to write mods, or any Java Application really, we need to download the
JDK. For Minecraft we need the JDK 8.

### Download

Since I am not a big fan of Oracle - the company creating and building Java -
and the way they handle the copyright licenses for their JDK, I use the
[AdoptOpenJDK 8, HotSpot](https://adoptopenjdk.net/) for this tutorial.
You can still download the JDK at the official
[Oracle site](https://www.oracle.com/java/technologies/javase/javase-jdk8-downloads.html).
Oracle requires you to create an account to begin the download, so be warned!

Which JDK you choose doesn't make a difference. Once you downloaded it you will
get an installer that you'll have to run to install Java. It is important to
enable the Option *Add to PATH* to make things work.

### Verifying Java Installation
Open you command line tool:
 - Windows: `WIN+R` and then type `cmd`
 - Mac: Open `Terminal`

We now test our Java installation by trying to run the JDK program. We do this
with the flag `-version` which simply tells the JDK program to print out details
regarding the version of out Java installation to the console:

```
> java -version
```

should evaluate to
```
openjdk version "1.8.0_242"
OpenJDK Runtime Environment (AdoptOpenJDK)(build 1.8.0_242-b08)
OpenJDK 64-Bit Server VM (AdoptOpenJDK)(build 25.242-b08, mixed mode)
```
or something similar. As you can tell at the moment of the creation of this
tutorial, version 1.8.0.242 was the newest.

If you have any problem regarding the installation or the console saying that
the program could not be found, make sure the `bin` folder in the Java
installation directory (on Windows usually found at
`C:\Program Files\AdoptOpenJDK` or `C:\Program Files\Java`) is added to the
`PATH` environment variable ([Windows](https://stackoverflow.com/questions/44272416/how-to-add-a-folder-to-path-environment-variable-in-windows-10-with-screensho)
| [Mac](https://stackoverflow.com/questions/7703041/editing-path-variable-on-mac)).

### ~ Now You!
I doubt you did just read through this, did you? You obviously installed Java
along the way. While this is nice and good for these installations in this
tutorial I want to ask from you to read through these sections first and
understand what I did before copying it and doing it yourself. This makes
your understanding of the things better and your learning easier.

**Task:** Install Java - if you didn't already.

#### Solution
The command `> java -version` in the command line should successfully return
some version information that says `version 1.8`.


## 2. Installing an IDE
We need a program to write the code for our mods or other applications you
may want to build. Since Java files are nothing other than normal text files
with a `.java` ending, we could theoretically write them with any text editor.
But to make life easier there are programs out there that support us in our
coding, developing and testing - *integrated development editors*. For Java
there are two *main* editors: **IntelliJ IDEA** and **Eclipse**. Personally,
I prefer IntelliJ and will be using IntelliJ for this tutorial but you can still
use Eclipse and be perfectly fine. The choice of your tools has no impact on
any following steps (besides one, but I'll say it there).

### ~ Now You!
**Task:** Install any IDE ([IntelliJ](https://www.jetbrains.com/idea/download/)
| [Eclipse - For Java Developers](https://www.eclipse.org/downloads/)) on your
local machine.


## 3. Installing the Forge MDK
The Forge MDK - short for Mod Development Kit is our primary tool to create
Forge Mods. We can download the MDK on the official website. The download will
be a zipped folder.

### Setting up the Workspace
We now move our Forge MDK zipped folder into a separate Forge development
directory. I usually like to store a folder named `Minecraft Forge` in my
`Development` folder but you may do as you wish. We extract the folder
(I usually name it after the Minecraft version only, so `Forge 1.15.2`).
This is our base folder for every mod we create.

Now we have to setup this folder for our IDE. The instructions for this are
also in the `README.txt` but I will still go over this.

#### IntelliJ
Open IntelliJ and click on `Import project`. Then navigate to the Forge folder,
select the `build.gradle` file and click on `OK`. Intellij will make the project
ready for you. Be patient while it downloads necessary assets and such.

Once it is finished, there is a `Gradle` tab on the right side of IntelliJ
Click it and run `Tasks > fg_runs > genIntellijRuns` to finish project setup.

Then refresh the project by clicking the refresh button.

#### Eclipse
Open the command line in the forge folder and run:
```
> gradlew genEclipseRuns
```
Be patient while it downloads and installs necessary files.

Then open Eclipse and set your Forge development folder as your workspace - you
can also make it the default one if you like to. Then import your project by
clicking on `File > Import > Gradle > Existing Gradle Project`. Then select the
Forge folder with the `build.gradle` in it.


### Testing our Mod
Forge comes bundled with an example mod. To verify that everything is correctly
set up, we will try and run this test mod and see if it shows up in the client.

The Forge project has therefor given us some pre-built configurations for the
IDE we are using. They are `Run Client`, `Run Server` and `Run Data`. We'll
only need `Run Client` for now. It starts Minecraft with Forge and the current
mod installed on a development account. We just need to run this, then Forge
will build the code we've written in the `src` folder into a `.jar` file that
then can be accepted by the Mod Loader as a mod.

**On IntelliJ:** On IntelliJ we have to specify which module we want Forge to
build. This is because IntelliJ projects are usually structured into `main` and
`test` sections - for the program and the program tests accordingly. Therefor
we need to edit the Run Configurations:
`Run > Edit Configurations > runClient > Use classpath of module: xxx.main`
We can than save this by clicking on `OK` and run it by selecting the profile
on the top right corner and clicking the play button.

**On Eclipse:** On Eclipse we can run the client directly. We just have to set
it to our default Run Configurations once:
`Run > Run Configurations > Java Application > runClient`. The next runs can
be initiated by pressing the green play button.

Minecraft will open up and under the Mods menu you'll find the standard Forge
example mod:

![Forge Example Mod](chapter2/examplemod.png)

### ~ Now You!

#### Tasks
 - [ ] Set up your Forge MDK
 - [ ] Accustom yourself to the use of your IDE (open some files and look at
   them)
 - [ ] Edit the configurations
 - [ ] Run Minecraft via the configuration and confirm your mod

#### Hints
 - If you run into any issues or errors while installing, feel free to contact
   me via e-mail (pythonit01@gmail.com)

#### Solution
You should see something very similar to the screenshot above.
