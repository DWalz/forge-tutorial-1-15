# Our first Block

Now that we successfully created an item we want to move on to blocks. Blocks
are a little more complicated in their creation, the main reason being that
the `Item` of the block that you hold in your inventory isn't the same as the
actual block placed in the world. There is a distinct difference between both
of them



## `Block`s, `BlockItem`s and `Item`s

`Block`s in Minecraft are the blocks we place in the world. The 3D model of a
block with surfaces in each direction is the most iconic feature of Minecraft.
The blocks you hold in your inventory are something different. The difference
isn't that hard to explain - especially if you remember what you learned about
`Item`s and `ItemStack`s in the last chapter.

The block in your inventory is an `ItemStack`. A collection of instances of a
certain `Item`. And you usually want to give your `Block` other properties than
your `Item`. The sapphire block we're about to build will have a specific
hardness for example while the sapphire block *item* in our inventory will have
other properties like the maximum stack size.

To bind `Block` and `Item` together and make it easier for us to create both
the `Block` and the `Item` (these need to be registered separately), Forge
gives us the helper class `BlockItem`.

Usually if you right-click with an `Item` nothing happens. This is because the
standard items in Minecraft have no special interactions (think diamond or our
sapphire for example). If we right-click an `Item` that is a block we usually
expect it to be placed as a block in the world. This is exactly where block
items come in. This class binds such events as the right click to a block place
and all that comes with it.

So `BlockItem`s are pretty useful. They take some redundant work from us while
also allowing for configurability. But we aren't required to use them. We could
just as well create our own right click event to our item and place the
respective block in the world.



## Creating and Registering a `Block`

Creating a `Block` is almost the same as creating an `Item`. We have the
default constructor

```java
new Block();
```

that we need to pass the block properties. The only difference is, that we need
to instantiate the `Block.Properties` by using an `Material`.

`Material`s are somewhat a part of a `Block`'s properties. They define the most
basic properties like if the `Block` blocks movement, if it requires a tool
to break or if it gets pushed by pistons. There are many `Material`s already
predefined by Minecraft - we usually can pick out the one we need. It also
influences how the block sounds. Breaking or placing sounds are determined by
the material (by default - you can create custom materials). A full list
of `Material`s and their properties is [here](). TODO: Insert Link

We want to create a sapphire block. The material that has the optimal standard
properties is `IRON`:

```java
new Block(Block.Properties.create(Material.IRON));
```


### Registering `Block`s

Blocks have to be registered the same way as items. Therefor we create another
`EventBusSubscriber` called `BlockInit` with a function subscribing to the
`RegistryEvent.Register<Block>` event.


### ~ Now You!

 - [ ] Create the class `BlockInit` in the package `...common.blocks` and make 
 it an `EventBusSubscriber`.
 - [ ] Create a `public static final Block SAPPHIRE_BLOCK` with the `IRON`
 `Material` and register it using the registry's `registerAll()` function with
 registry name `sapphire_block`.

#### Hints

 - This class is very similar to the `ItemInit` class. You should have no
 difficulties making it.

#### Solution

```java
@Mod.EventBusSubscriber(modid = TutorialMod.MOD_ID, bus = Mod.EventBusSubscriber.Bus.MOD)
public class BlockInit {

    public static final Block SAPPHIRE_BLOCK =
            new Block(Block.Properties.create(Material.IRON)).setRegistryName("sapphire_block");

    @SubscribeEvent
    public static void registerBlocks(RegistryEvent.Register<Block> event) {
        event.getRegistry().registerAll(
            SAPPHIRE_BLOCK
        );
    }

}
```


### Block models and resources

The approach of adding block data to the mods data pack is a little different
from adding items. A block in Minecraft can have several models, based on it's
block state. Block states are different states a block can be in. Each block
state has it's own model.

Furnaces, for example, have two different block states: On and Off. Depending
on the block state, the furnace has a different model - one in which the fire
is lighted and one in which it isn't. Other prominent examples are: doors,
redstone torches or pressure plates. Each one of them looks different when
in different states.


#### Adding block states to the resource pack

Block states are stored in a folder `blockstates` in the resource pack - in our
case in `assets.tutorialmod.blockstates`. The block states file is named the
same as the blocks registry name and can point to several models. Each different
block state is stored in a `variants` object. We will discover later how
block states work, for now just keep in mind that if there is one unnamed
variant it's model will be used for every block state.

```json
{
  "variants": {
    "": { "model": "tutorialmod:block/sapphire_block" }
  }
}
```

This will reference each block state to the model defined in 
`tutorialmod.models.block/sapphire_block.json`.


#### Adding a block model

Block models mostly work the same as item models. The only big difference is, that you
usually want to make them inherit from `block/cube_all` which means a cube on all 
sides. If you want to create more complex models for both items and blocks, you'll
usually use a program that generates all the files for you. But since we want to stick
to the basics, we'll create them ourselves.

The textures of `block/cube_all` aren't organized in layers like the ones in items,
they are organized in sides. Our sapphire block will have the same texture on each
side so we can just use `all` to show we want this specific texture to appear on each
side of the block.

```json
{
  "parent": "block/cube_all",
  "textures": {
    "all": "tutorialmod:block/sapphire_block"
  }
}
```

This points the texture to an image in 
`tutorialmod.textures.block/sapphire_block.png`


#### Adding a block texture

Adding the block's texture is now easy and the same as before with the sapphire gem.
We want to create a file called `sapphire_block.png` in the folder
`tutorialmod.textures.block`. Since I really liked the look of the old emerald texture
in Minecraft, I decided to go along with it and recolor and tweak it a little. You can
just download it 
[here](https://docs.zoho.com/file/kvqd57a47cbf4769744a7a173fefa86dda465).

![My emerald block texture](./chapter6/emerald_block_texture.png)


#### ~ Now You!

 - [ ] Create the `blockstates` folder and add the block's block state file
 (named `sapphire_block.json` (the blocks registry name))
 - [ ] Add the sapphire block model and it's texture
 - [ ] Verify that your block works and has the right model


##### Hints

 - If you got something wrong, don't worry. Continue with the next part of the tutorial
 and download the source code at the end of this chapter to see what you did wrong
 - **Important:** you can't get the block somewhere in the inventory since we haven't 
 yet registered any `Item` that you can hold. Place your block inside the world by 
 using the following command: `/setblock ~ ~ ~ tutorialmod:sapphire_block`


##### Solution

There is no real solution other than seeing the block working in game. Resource files
have to be exactly right in order to work properly. All the content of the files are
above. Copy and paste if your's doesn't seem to work. Otherwise copy the whole assets
folder from the source download at the end of the chapter (after finishing it).



## Creating and Registering the `BlockItem`

Now that we have our `Block` finished, we can take it and get our `BlockItem` from it.
Doing so is fairly simple. The registration works just like normal items - in fact we 
will just use the `ItemInit` class to hold and register it. The only thin being
slightly different is the initialisation of the `Item`.


### Getting a `BlockItem` from a `Block`

... is pretty easy. The class `BlockItem`s constructor requires us to give it a `Block`
to even create an instance. So

```java
new BlockItem(BlockInit.SAPPHIRE_BLOCK, new Item.Properties());
```

will give us the `BlockItem` corresponding to the `SAPPHIRE_BLOCK` in the `BlockInit`
class. We can modify the `BlockItem`'s properties just like we could modify it for any
other `Item`. We could - for example - put it into our mod's `ItemGroup` just like the
gem. In order to not confuse ourselves and players in game, we want to keep the 
registry name the same for both `Block` and `BlockItem`.

The reason for this is pretty simple - in fact, we even used it in the *hint section*
of the last **~ Now You!**: Commands. If our registry names were different, you'd have
to use two different names when using the command `/give` and the command `/setblock`
or `/fill`. 

These two different names won't lead to namespace conflicts since Minecraft 
distinguishes strictly between blocks and items. This is evident when we try to name
our block later. The commands process this under the hood. `/give` will only search
in the `item` namespace where as `/setblock` or `/fill` will search for `block`s 
specifically (This also happens with other commands such as `/playsound`, more later).

We can easily grab the `Block`'s registry name and reuse it here by using the 
`getRegistryName()` function:

```java
new BlockItem(BlockInit.SAPPHIRE_BLOCK, new Item.Properties())
        .setRegistryName(BlockInit.SAPPHIRE_BLOCK.getRegistryName());
```

### ~ Now You!

 - [ ] Create a `public static Item SAPPHIRE_BLOCK` in the `ItemInit` class that uses
 the `BlockItem` constructor to build the `Item` from `BlockInit.SAPPHIRE_BLOCK`.
 - [ ] Set the `ItemGroup` to out mod's `ItemGroup` and the registry name to the
 `BlockInit.SAPPHIRE_BLOCK`'s registry name. Register the item in the register
 function.


#### Hints

 - The final `Item` should look very similar to the `SAPPHIRE` `Item`.


#### Solution

This one shouldn't be too hard:

```java
@Mod.EventBusSubscriber(modid = TutorialMod.MOD_ID, bus = Mod.EventBusSubscriber.Bus.MOD)
public class ItemInit {

    public static final ItemGroup MOD_GROUP = new ItemGroup("tutorialmod") {
        @Override
        public ItemStack createIcon() {
            return new ItemStack(SAPPHIRE);
        }
    };

    // ITEMS
    public static final Item SAPPHIRE =
            new Item(new Item.Properties().group(MOD_GROUP)).setRegistryName("sapphire");

    // BLOCK ITEMS
    public static final Item SAPPHIRE_BLOCK =
            new BlockItem(BlockInit.SAPPHIRE_BLOCK, new Item.Properties().group(MOD_GROUP))
                    .setRegistryName(BlockInit.SAPPHIRE_BLOCK.getRegistryName());

    @SubscribeEvent
    public static void registerItems(final RegistryEvent.Register<Item> event) {
        event.getRegistry().registerAll(
                SAPPHIRE, SAPPHIRE_BLOCK
        );
    }

}
```

### `BlockItem`'s item model

If you try to start the game now and get yourself a sapphire block, you'll notice that
it still has no model - even though the block itself has one. This is because even with
`BlockItem`s, the item has it's own model. 

This can be useful and there are some in-game examples where the item model is
different to the block model, one of them beingredstone dust (Yes, placed redstone dust
is counted as a block).

Mostly, however you just want a 3D-like item of the block. You can just give a 
reference to another model as parent - with or without adding any extra modelling - and 
this model will be used instead. So in our case we can just use
`tutorialmod:block/sapphire_block` as parent and the block's in game model will be
used:

```json
{
  "parent": "tutorialmod:block/sapphire_block"
}
```


#### ~ Now You!

 - [ ] Create the block item's model file (called `sapphire_block.json` - the registry 
 name) and bind it to the block's model as parent
 - [ ] Name the block in your language file


##### Note
 - Start the game to see what the language string should be.


##### Solution

`tutorialmod.models.item/sapphire_block.json`:

```json
{
  "parent": "tutorialmod:block/sapphire_block"
}
```

`tutorialmod.lang/en_us.json`:

```json
{
  "itemGroup.tutorialmod": "Tutorial Mod",
  "item.tutorialmod.sapphire": "Sapphire",
  "block.tutorialmod.sapphire_block": "Sapphire Block"
}
```



### Letting the block drop - loot tables, hardness and harvest levels

If you try to break some sapphire blocks in survival mode, you'll probably notice that
they are destroyed instantly. It doesn't matter if you break them by hand or with a
pickaxe. They just break, still making the breaking sound, but the block doesn't drop.


#### Loot tables

The first thing we want to do is making the block drop. This is where loot tables come
in. Loot tables are located in the game's **data pack** (we worked in the resource 
pack up until now). Loot tables define how big of a chance what items spawn on certain
events or conditions. An easy to understand example for loot tables would be dungeons:
How many and which items are in dungeon chests is solely defined by a loot table file
which tells the game how much and how big of a chance.

The same happens with block breaks. When a block is broken *successfully* - that means
it is broken with the right tools - and an item drop is expected to happen, the game 
looks up the loot table for the block and spawns items accordingly.

To create our mod's data pack, we have to create the folder `data.tutorialmod` in the 
`resource` folder - parallel to the `assets` folder. All loot tables are collected in a
folder called `loot_tables`, those for block breaks in a subfolder called `blocks`.

So in order to create a new loot table for our block, we have to create a file called
`sapphire_block.json` in the `data.tutorialmod.loot_tables.blocks` folder. 

Each loot 
table defines the `type` of item spawn event it refers to - in our case 
`minecraft:block`. We then have a list of `pools`, each of them has a certain number of
rolls - if the event occurs, each spawn pool enters with it's number of rolls. Pools
can also have conditions and apply functions to the items spawned. Each pool then has
it's `entries`: The items generated if this pool is rolled. 

Since this is hard to understand in text, we'll just look at the loot table for our
block:

```json
{
  "type": "minecraft:block",
  "pools": [
    {
      "rolls": 1,
      "entries": [
        {
          "type": "minecraft:item",
          "name": "tutorialmod:sapphire_block"
        }
      ],
      "conditions": [
        {
          "condition": "minecraft:survives_explosion"
        }
      ]
    }
  ]
}
```

The `type` defines that we want this loot table to be called on block break. We then
define our pools. 

Since we only want the block to drop, we can set it's rolls to `1`.
This means that there is a total of `1` rolls and our pool has `1` roll in it - the
pool will be chosen to 100%. If we'd create a second pool with `1` roll, we would have 
a total of `2` rolls - the probability of each of them being chosen would be 1 out of
2 or 50%. 

We then define the `entries` of our pool. This tells the pool which and how many of
each entities we want to spawn. This doesn't have to be only items, it could also be
other entites like monsters - silverfish spawn in Minecraft after you break certain
stone blocks. The only entry of our pool is the item of the block - we want the player
to get the block's item after all. 

In the `conditions` list you can define extra conditions. I defined one to be
`survive_explosion`. This means the block's item will still drop if destroyed by an
explosion.

There is still much more to learn about loot tables but we don't need it just yet. If
you are still interested in knowing more, you can look 
[here](https://minecraft.gamepedia.com/Loot_table) to find a full list of what you can
do with them.


#### Hardness and resistance

The second problem we have is that we don't want the block to break instantly. In 
Minecraft the time it takes to break a block is determined by the hardness of the block
and can be accelerated by the tool the player is holding. The hardness values are some
numbers and can be set in the block's `Properties`. Usually it's best to orient
yourself at the hardness values of other blocks. A list can be found 
[here](https://minecraft.gamepedia.com/Breaking#Blocks_by_hardness).

I'll set my hardness value of the sapphire block to `5`, the same hardness value as 
both a diamond and an emerald block:

```java
new Block(Block.Properties.create(Material.IRON).hardnessAndResistance(5))
            .setRegistryName("sapphire_block");
```

Resistance is a value that determines how hard it is to break the block by explosions.
For the most blocks this value is the same as the hardness value. There are exceptions,
obsidian for example doesn't break through explosions - it has a resistance value of
`Infinity` but can still be broken by mining it. 


#### Harvest level and tool type

The harvest level defines which level of tools is needed to break the block 
successfully. It doesn't influence the tool type - that is done by `harvestTool()`.
The harvest level only specifies the tool tier. There are currently `4` harvest
level in the game (1.15.2), the netherite tools in 1.16 are a `5`th. Wood and gold
tools have a harvest level of `0`, stone `1`, iron `2` and diamond `3`.

I want my block to be broken by harvest tier iron and up, so I'll set the value to 2.
The respective function is `harvestLevel()`.

The type of tools I want the block to break is pickaxes. The standard properties of a
block set the tool type to `ToolType.PICKAXE` which is the right one for us but it
still dooesn't do any bad to specify it extre:

```java
new Block(
        Block.Properties.create(Material.IRON)
                .hardnessAndResistance(5)
                .harvestLevel(2)
                .harvestTool(ToolType.PICKAXE)
        ).setRegistryName("sapphire_block");
```


#### ~ Now You!

 - [ ] Create the loot table for the sapphire block
 - [ ] Change the sapphire block's properties so it has a hardness of 5, a harvest 
 level of 2 and can be broken by pickaxe.


##### Hints 
 - Test if it works by trying to break the block in survival mode and see if it drops
 with the right tools


##### Solution

The block definition (in `ModBlocks`):

```java
public static final Block SAPPHIRE_BLOCK =
        new Block(
                Block.Properties.create(Material.IRON)
                        .hardnessAndResistance(5)
                        .harvestLevel(2)
                        .harvestTool(ToolType.PICKAXE)
        ).setRegistryName("sapphire_block");
```

The loot table file (`data.tutorialmod.loot_tables.blocks/sapphire_block.json`):
```json
{
  "type": "minecraft:block",
  "pools": [
    {
      "rolls": 1,
      "entries": [
        {
          "type": "minecraft:item",
          "name": "tutorialmod:sapphire_block"
        }
      ],
      "conditions": [
        {
          "condition": "minecraft:survives_explosion"
        }
      ]
    }
  ]
}
```



## Refactoring

A developer likes his code clean. Over the course of writing software you'll create
many messy corners in your code. It helps you and other if you go back once in a while
and clean up. This is the reason why I want to refactor and refine my code with you:
To help you see what is unecessary or bad practice and improve on it until it looks
better.

If you take a look at our way registering stuff you'll find out that both item and
block class look pretty similar. And when we create other stuff than block and items
like potions or tools, we'll need to use other registries and other registration or
event subscribing functions. 

This is why I'd split registration and storing things: One class called `ModRegistry`
in the `util` package that subscibes to all `RegistryEvent.Register<>` events and
the two classes `ModBlocks` and `ModItems` in the `common` package that store all 
blocks and items. Both packages `blocks` and `items` will become unnecessary for now
but we'll use them later if we create specific block or item classes.

I also want the items and blocks to be in an array in their own respective classes and
just feed that whole array into the registry. That way we can create items in their
class and change the item list in the same class without having to touch the 
`ModRegistry` class ever again.

This section doesn't have a now you part but if you want to try and make those changes
yourself feel free to do so. Otherwise: Here are my new classes:

`tutorialmod.util.ModRegistry`:

```java
@Mod.EventBusSubscriber(modid = TutorialMod.MOD_ID, bus = Mod.EventBusSubscriber.Bus.MOD)
public class ModRegistry {

    @SubscribeEvent
    public void onBlockRegister(RegistryEvent.Register<Block> event) {
        event.getRegistry().registerAll(ModBlocks.BLOCKS);
    }

    @SubscribeEvent
    public void onItemRegister(RegistryEvent.Register<Item> event) {
        event.getRegistry().registerAll(ModItems.ITEMS);
    }

}
```

`tutorialmod.common.ModBlocks`:

```java
public class ModBlocks {

    // BLOCKS
    public static final Block SAPPHIRE_BLOCK =
            new Block(
                    Block.Properties.create(Material.IRON)
                            .hardnessAndResistance(5)
                            .harvestLevel(2)
                            .harvestTool(ToolType.PICKAXE)
            ).setRegistryName("sapphire_block");


    // BLOCK LIST - for registry
    public static final Block[] BLOCKS = {
            SAPPHIRE_BLOCK
    };

}
```

`tutorialmod.common.ModItems`:

```java
public class ModItems {

    // ITEM GROUP
    public static final ItemGroup MOD_GROUP = new ItemGroup("tutorialmod") {
        @Override
        public ItemStack createIcon() {
            return new ItemStack(SAPPHIRE);
        }
    };


    // ITEMS
    public static final Item SAPPHIRE =
            new Item(
                    new Item.Properties().group(MOD_GROUP)
            ).setRegistryName("sapphire");


    // BLOCK ITEMS
    public static final Item SAPPHIRE_BLOCK =
            new BlockItem(
                    ModBlocks.SAPPHIRE_BLOCK, 
                    new Item.Properties().group(MOD_GROUP)
            ).setRegistryName(Objects.requireNonNull(ModBlocks.SAPPHIRE_BLOCK.getRegistryName()));


    // ITEM LIST - for registry
    public static final Item[] ITEMS = {
            SAPPHIRE, SAPPHIRE_BLOCK
    };

}
```



## Looking back

This chapter's intent was primarily to teach you how blocks work in **Minecraft**, not
how they are added in Forge. The registering and such was very similar to items. The
important concepts were those of differentiating between blocks, block items, items
and item stacks. I also hope you learned something about Minecraft's resource and data
packs as well.

I really enjoy making this tutorial and I hope it is an as refreshing and joyful
journey for you as it is for me. If you have any comments, questions or ideas, feel
free to always contact me.


## Source code

You can find the full git project [here]().
Or you can download an archived version directly from 
[here](https://docs.zoho.com/file/kvqd57db3c49c089a43e09aa77a1370313d75)