# Chapter 4 - Our first Item!
Okay, so now that we have a completely empty Mod that does absolutely nothing,
we have to implement stuff that extends out Minecraft experience. And we most
certainly want to have new items to play and craft with. As mentioned before,
I am building a Mod that adds sapphire stuff into the game so our first item
will obviously be a sapphire.

But before we jump directly to work, we have to speak about an important
concept for Minecraft: **Siding**.
And about one for Forge: **Registries**.


## 1. Siding
In Minecraft, we usually distinguish two *sides*: **client** and **server**.
And these two are not, what you first think of them. Even in a singleplayer
world you have a server, not just a client. This is because of how we define
client and server.

### Server
The server is everything that handles the game logic. It is responsible for
weather, player data, mob spawning and many more tasks. The server is more or
less what makes Minecraft a game, what transforms the inputs of the player
into events that change the game.

### Client
The client is that part of Minecraft, that accepts inputs from the player and
gives them to the server to handle and respond to. It then takes the response
from the server and visualizes it back to the player.

So when we begin breaking a block in Minecraft by hitting a block, the client
relays that left-clicking to the server. The server then uses the information
it has about the player and creates a response - usually displaying the break
state of the block and dropping it once it has been mined with the right tool.
The client updates the screen based on that and displays the cracks in the block
and the dropped item.  

So a normal single player world consists of both client and server. We can
interact with it but it also manages the game logic. The client sometimes also
is referred to as the UI, that part that the user sees and the game logic runs
behind the scenes to calculate what the player is seeing.

### Why do we need this
Usually, most of the stuff we do will influence both client and server. If we
add a new item, the client needs to know how to display the items, which
animations and sounds are connected to it and so on. The server also needs to
know stuff about the item, prominent example: Crafting. The server needs to
know in which crafting recipes the item is used to handle the logic once a
player has put that item into a crafting bench. If something is used by both
sides we usually put it in a package named `common`.

If things are only client-sided we use `client` and when they are server-sided
`server.dedicated`. These names are not just there to confuse developers, these
are the sidings used in Minecraft and their respective package names.


## 2. Registries
Registration is what takes an object we want to have in our Mod and make it
know to Minecraft so the client and the server can interact with it. Almost
anything that we'll want to register can be registered with the Forge
Registries. A registry saves an object to an ID so the registered objects can
be identified uniquely. We before had an example of such a registration process:
The Mod (modloading) itself. We identified it with it its ID.

### Registering things
We've learned that Forge uses dedicated Registries to register objects. For that
the main Event Bus fires a registry event that indicates that the registry is
now registering every object of a certain registry type. There are many registry
types for things such as `Block`, `Item` or `Potion`. The registry fires for
example a `RegistryEvent.Register<Item>` event if it now registers all the
items. We can subscribe to that event and when it runs give the registry all
items we have in our mod for it to register them.

So how would be the most basic way to register our sapphire item?

## 3. Registering our own `Item`

### Creating a basic `Item`
The most basic item we can create is just an instance of the `Item` class.
Later we will want to make our Item have some special behavior but the sapphire
gem doesn't have to do anything. It just has to be there:

```java
new Item(new Item.Properties());
```

This is the most basic `Item` we can create. It has the default
`Item.Properties`. We will later want to modify it but for now we just want it
to be registered.

### Registering the `Item`
To register an Item or really anything, we need to subscribe a function to the
`RegistryEvent.Register<Item>` event. To keep our Mod's main class as empty as
possible we'll create a new class `ItemInit` in a subpackage `common.items` of
our Mod. To tell Forge that in this class there are function listening to the
event bus and eventually subscribing to events the bus fires we'll have
to **annotate** this class. The Annotation that tells Forge that we have
subscribing functions is: `@Mod.EventBusSubscriber(value, modid, bus)`:

 - `value` defines on which side we want to load the event subscriber on. Since
   we want to create an `Item` that needs to be known for both client and
   server - which is the default value - we'll leave this empty.
 - `modid` is the ID of the subscribing mod - in our case `tutorialmod`.
 - `bus` is the bus we want to subscribe to. There are two: `FORGE` for
   Forge-related subscriptions (we'll learn about that much later) and `MOD` for
   Mod related subscriptions. Since we want to register an `Item` in our Mod,
   we'll obviously use the Mod-specific event bus.

This gives the following class:

```java
@Mod.EventBusSubscriber(modid = "tutorialmod", bus = Mod.EventBusSubscriber.Bus.MOD)
public class ItemInit {

}
```

Now we need to create the subscribing function. We can mark it as a subscribing
function by annotating it with `@SubscribeEvent`. The subscribing function
must be a function that has only one parameter that is a subclass of `Event`.
`RegistryEvent.Register<Item>` is one such class. I will name my function
`registerItems`:

```java
@Mod.EventBusSubscriber(modid = "tutorialmod", bus = Mod.EventBusSubscriber.Bus.MOD)
public class ItemInit {

  @SubscribeEvent
  public static void registerItems(RegistryEvent.Register<Item> event) {

  }

}
```

When this function gets called, the Registry is now registering `Item`s. We can
therefor pass it `Item` instances to register them. We'll pass it our simple
`Item` we created earlier for now. To register an instance we need to get the
registry of the event and then pass the instance to it's `register` method:

```java
@Mod.EventBusSubscriber(modid = "tutorialmod", bus = Mod.EventBusSubscriber.Bus.MOD)
public class ItemInit {

  @SubscribeEvent
  public static void registerItems(RegistryEvent.Register<Item> event) {
    event.getRegistry().register(new Item(new Item.Properties));
  }

}
```

This tells the registry to register our item. But it still won't work. Can you
think of why? Take a moment to think about it again and maybe take a look at
the first paragraph in the second section.

Have you found the solution? Congratulations, if so, don't worry though if you
didn't. The error is almost the same as the one we had when we first tried to
start our mod without the correct ID. Every thing has to be registered to an ID
and where from does the registry know which one we want the `Item` to have?

We specify the ID by giving the `Item` we want to register a registry name. This
is identical to it's id:

```java
@Mod.EventBusSubscriber(modid = "tutorialmod", bus = Mod.EventBusSubscriber.Bus.MOD)
public class ItemInit {

  @SubscribeEvent
  public static void registerItems(RegistryEvent.Register<Item> event) {
    event.getRegistry().register(new Item(new Item.Properties).setRegistryName("sapphire"));
  }

}
```

### Register our Mod as Subscriber to the Forge Event Bus
In order for Forge to even search our Mod's classes for subscribe annotations
we have to tell Forge at mod construction time that our Mod wants to be
registered to the Forge event bus. We'll do this in our Mod's main class
constructor. The way to do it is:

```java
@Mod("tutorialmod")
public class TutorialMod {

    public TutorialMod() {
        MinecraftForge.EVENT_BUS.register(this);
    }

}
```

This tells Forge's event bus that `this` (the instance of the Mod) has
registered itself to receive, modify and fire bus events.

### Trying it out

If we now start Minecraft we won't be able to see the item in the creative menu
and in the world or anywhere.

![Try to find item](chapter4/itemsearch.png)

But we can verify that it has been registered if
we just try to give it to us.

![Try to give item](chapter4/itemgive.png)


### ~ Now You!

#### Tasks
 - [ ] Create the class `ItemInit` and make it an `EventBusSubscriber`
 - [ ] Create the function `registerItems` and make it subscribe to the
   `RegistryEvent.Register<Item>` event.
 - [ ] Register a new item with the registry name `sapphire`
 - [ ] Test the Mod and give yourself that item. What name does it have?
   What texture does it have? Why could that be the case?

#### Hints
 - The code for all the class and the function is completely above. Try to
   understand what you are doing and why you are doing it.
 - The weird name comes from the registry name we have given the Item.
 - The texture is Minecraft's basic texture for undefined stuff. Maybe you've
   seen it before in old texturepacks or wrong texturepack versions.

#### Solution
 - The complete code of the `ItemInit` class:
 ```java
 @Mod.EventBusSubscriber(modid = "tutorialmod", bus = Mod.EventBusSubscriber.Bus.MOD)
 public class ItemInit {

     @SubscribeEvent
     public static void registerItems(final RegistryEvent.Register<Item> event) {
         event.getRegistry().register(new Item(new Item.Properties()).setRegistryName("sapphire"));
     }

 }
 ```
 - And the `TutorialMod` main class:
 ```java
 @Mod("tutorialmod")
 public class TutorialMod {

     public TutorialMod() {
        MinecraftForge.EVENT_BUS.register(this);
     }

 }
 ```
 - The name points to a string in the language file. When Minecraft has to
   display the item it tries to get the name from that file. Since we have
   neither created that file yet nor defined
   the string there (we will do so next section) the reference name
   itself is displayed. This is useful for finding the right keys in the
   language files.
 - We also haven't specified a model or a texture yet. We'll do that
   in the next section as well.


## 4. Fixing Name and Texture
We previously successfully registered our item. But it still doesn't look like
a good item. Since we don't want it to have extra functionality on itself -
besides that we'll want to craft it into a block, but that is done by the
`Recipe` class - we won't touch this item registration anymore until we learn
some more things about the registration.

We now want to give our item a Texture and a name so it will be displayed
properly in game. Therefor we need to edit the Mod's **Resourcepack and
Datapack**. Datapacks save custom information like advancements, loot tables,
structures and many mode. Resourcepacks are basically special datapacks. They
save information on how objects are visualized, giving e.g. items model and
texture.

### Creating a resource pack
Every resource pack is just a special datapack. Therefor we will look at the
basic construction of a datapack and then forge that into a resourcepack.

A datapack consists of two parts: a `pack.mcmeta` and the `data` folder.
If you remember back to the creation of our mod, we already had a `pack.mcmeta`
file in our `resource` package. This is the standard datapack of our mod. So
in order to create a datapack we'd just have to add the `data` folder. Since
we want to create a resourcepack this folder wont be named data but `assets`.

This part of the item creation is very much similar to the remodeling of
vanilla items so I'll refer to the Minecraft Gamepedia site from now on:
[Resourcepack site](https://minecraft.gamepedia.com/Resource_pack).

### How a resource pack is built
Here is how the folder structure of a resource pack looks like - note: this is
only regarding the basic structure for items and blocks. We'll add more and
more folders once we are creating other things:

```
|-pack.mcmeta
|-assets
  |-(namespace - in our case the modid: tutorialmod)
    |-lang
    |-models
    | |-block
    | |-item
    |-textures
      |-block
      |-item
```

In the `lang` folder we save out text translations - this is how we adjust the
display name of the item for example

In the `models` folder there will be our model data. Models are defining how
items are represented in 3D. There can be different layers that modify our item
and make it look really detailed. For now we'll stick with the basics though.

In the `textures` folder are the textures for our models. They define what the
surface of an item will look like.

### Renaming out item
To rename our item we have to create a language file. A language file stores
**all** translations for every string in the game. Each string has a unique
identifier. The identifier of our sapphire item is `item.tutorialmod.sapphire`,
generally it is an identifier like `item` or `block` for which kind of object
the string describes followed by the Mod's ID and the ID of the object the
string goes to.

Language files are `JSON` files. This is a certain data format. It saves things
in objects, surrounded by `{}`. Data is stored in the `"key": "value"`
principle. Data can also be stored in arrays, surrounded by `[]`. Since this is
hard to imagine, the best thing we can do is create such a `JSON` file. We want
to provide the English translation therefor we name it `en_us.json`.

Since everything is stored in objects, even the top-level data, we create an
anonymous object first:

```json
{

}
```

It is anonymous because we didn't assign it to any key.

Now we want to save the translation of our item name to this object. That is
really simple: We just register that key - value pair in this anonymous object:

```json
{
  "item.tutorialmod.sapphire": "Sapphire"
}
```

If we now start the game and give ourselves this item, it'll have the name
`Sapphire` and no longer `item.tutorialmod.sapphire`

### Creating Item Model and Texture
We now want to make our sapphire look like one. Right now it has Minecraft's
default for when no texture is available. We'll have to define first the model
and then the models texture to give the sapphire item a different look.

#### The Sapphire Model
Item models are used to depict the items in the game. The look in the players
hand, on the ground, in item frames or many more is defined by our model.

##### How do Item Models work
Item models are built in a certain way. For a full documentation look at the
[Gamepedia site](https://minecraft.gamepedia.com/Model#Item_models). Item models
also are `JSON` files with a certain structure. This structure contains many
modifiable tags, I'll present the ones that could be important for us here:

```json
{
  "parent": "",
  "textures": {
    "layer0": "...",
    ...
  },
  "elements": []
}
```

We can think of a model as a combination of cubes of many sizes. Each of the
faces of the cube has a certain texture. The model is the combination of all
these cubes. The standard Minecraft model derives its cubes from the texture
image. We will mostly use that feature but I am planning to create a full model
later. Each pixel on the texture file is converted into a cube with the color
of the pixel. This is then combined into the full item model. To use this
standard model we can use the `parent` tag and set it to a value of
`item/generated`. This tells Minecraft to generate the item model from the
`level0` texture of the `textures`.

In the `elements` tag we could specify which cubes we want to have and where and
with what texture. This is used to create more complex item models. Since we
use the generated models we won't need that feature for now:

![Item model](chapter4/itemmodel.png)


##### Create our Sapphires model
Since we want to use the generated model, the `JSON` file of our model will
be pretty simple. It has to be named the same as the items registry name
(in our case `sapphire`) with the ending `.json`. In it will only be the
folowing:

```json
{
  "parent": "item/generated",
  "textures": {
    "layer0": "tutorialmod:item/sapphire"
  }
}
```

The value of `layer0` comes from where we'll want to save our texture. We save
our texture in the `tutorialmod` package under the item textures in a file
called `sapphire.png`. Important: These files have to be 16x16 or 32x32 and
they have to be in the `.png` format.

#### The Sapphire Texture
The item model now renders based on the texture in `textures/item/sapphire.png`.
We now have to create this file. You can use any image editor for that, I
usually use Paint.net but every other image editor will just do fine.

We create a 16x16 image that will then be used as our image texture to
generate our model and will now represent the item in the game.


### ~ Now You!
 - [ ] Create the `assets.tutorialmod` package. In it create the `lang`,
   `models` and the `textures` folder. In the `models` and the `textures` folder
   create an `item` and a `block` folder each
 - [ ] Change the name of the sapphire item by adding the respective key
   in a new `en_us.json` file in the `lang` folder
 - [ ] Change the model of the item to be generated automatically and apply
   a texture named `sapphire.png` to it that is stored under `textures/item`
 - [ ] Create or copy a sapphire texture into previously mentioned location.
   You can use any texture that you can find online or the one
   [I created](https://docs.zoho.com/file/joi3k3c3c3d77fa974d83bb29a475b0bc6261)

   ![sapphire texture](chapter4/sapphire.png)



## 5. Looking back and refactoring

Since we've now completely created our first item and registered it to the
registry I now want to take a step back and look at what we've done and learned.

Forge is a vast tool and there are many more ways to do what we just did. We
registered an item by getting the registry Forge uses to add items into the
game and told it to register our sapphire. This just tells Forge that there is
an item with the properties we gave it. We haven't defined any special
functionality or assigned it to any item group.

We then added a model to our mods *data pack*. When trying to display our item,
Minecraft will actually look at the data pack and just take our defined models
and textures to create the 3D representation of it in the game.

But our mod won't only have one item. We want to add sapphire tools, armory,
blocks and some other stuff too. To make it easier for us and keep the code
cleaner, we're going to change a few things.


### The ModID

You'll probably have noticed: We use our mods id a lot. Every
`EventBusSubscriber` class has to give forge the mods id, for example.
If we hardcode it everywhere, changing will be very hard and tedious.

Instead, we should save it in the main mod class as a `static` variable to be
able to access it from anywhere while still allowing us to change it in one
place.


### The registry

Since we will be registering multiple items, I'd recommend grabbing the registry
once instead of every time we want to register a new one. It is a small thing
that'll gain us a lot of performance.


### ~ Now You!
 - [ ] Add a `public static String MOD_ID` to your main class and assign it the
       ID of our mod. Then replace all occurrences of the id string in java
       code with the new variable.
 - [ ] Grab the registry from the event (just like we did when registering
       the item) and save it in a variable. Then modify our existing
       registrations to use this registry variable instead of
       `event.getRegistry()`

#### Hints
 - Saving the ID is nothing more than adding
   `public static String MOD_ID = "tutorialmod"` to the beginning of the mods
   main class.
 - The `even.getRegister()` function returns an object of type
   `IForgeRegistry<T>`. In our case `T` is `Item` since we subscribe to a
   `RegistryEvent.Register<Item>` event.

#### Solution
Adding the static ID variable is pretty simple. Your main class should look
something like this:

```java
@Mod(TutorialMod.MOD_ID)
public class TutorialMod {

    public static final String MOD_ID = "tutorialmod";

    public TutorialMod() {
        MinecraftForge.EVENT_BUS.register(this);
    }

}
```

Saving the registry makes the code one line longer but will save us work in the
future (note, I've swapped the `modid` in this class' annotation too):

```java
@Mod.EventBusSubscriber(modid = TutorialMod.MOD_ID, bus = Mod.EventBusSubscriber.Bus.MOD)
public class ItemInit {

    @SubscribeEvent
    public static void registerItems(final RegistryEvent.Register<Item> event) {
        IForgeRegistry<Item> itemRegistry = event.getRegistry();
        itemRegistry.register(new Item(new Item.Properties()).setRegistryName("sapphire"));
    }

}
```

## ~ The project

You'll find, that I'll provide links to the contents of my `src` folder going
forward. This is so you have the full code with you at any time. Feel free to
download it and take a look at it.
